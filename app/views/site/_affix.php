<div class="bs-sidebar list-group subNav hidden-print text-right affix" role="complementary">
    <ul class="nav bs-sidenav">
        <li class="list-group-item">
            <a href="#alert">Alert</a>
        </li>
        <li class="list-group-item">
            <a href="#buttons">Buttons</a>
        </li>
        <li class="list-group-item"><a href="#charts">Charts</a></li>
        <li class="list-group-item"><a href="#forms">Forms</a></li>
        <li class="list-group-item"><a href="#mnav">Middlenav</a></li>
        <li class="list-group-item">
            <a href="#pagination">Pagination</a>
        </li>
        <li class="list-group-item">
            <a href="#progress">Progress</a>
        </li>
        <li class="list-group-item">
            <a href="#gridView">Tables</a>
        </li>
        <li class="list-group-item">
            <a href="#icons">Icons</a>
        </li>
    </ul>
</div>